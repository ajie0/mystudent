/*
SQLyog Community v13.1.7 (64 bit)
MySQL - 5.0.22-community-nt : Database - db_student
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;


CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_student` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `db_student`;

/*Table structure for table `achievement` */

CREATE TABLE `achievement` (
  `achievementid` int(11) NOT NULL auto_increment,
  `stuid` int(11) default NULL,
  `coursename` varchar(20) default NULL,
  `achievement` varchar(20) default NULL,
  PRIMARY KEY  (`achievementid`),
  KEY `stuid` (`stuid`),
  KEY `coursename` (`coursename`),
  CONSTRAINT `achievement_ibfk_1` FOREIGN KEY (`stuid`) REFERENCES `student` (`sid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `achievement_ibfk_2` FOREIGN KEY (`coursename`) REFERENCES `course` (`coursename`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `achievement` */

insert  into `achievement`(`achievementid`,`stuid`,`coursename`,`achievement`) values 
(1,1,'java','96'),
(19,1,'篮球',NULL);

/*Table structure for table `class` */

CREATE TABLE `class` (
  `classId` int(11) NOT NULL,
  `cclasname` varchar(20) NOT NULL default '',
  `teachername` varchar(20) default NULL COMMENT '班主任',
  PRIMARY KEY  (`classId`),
  KEY `teachername` (`teachername`),
  KEY `classname` (`cclasname`),
  CONSTRAINT `class_ibfk_1` FOREIGN KEY (`teachername`) REFERENCES `teacher` (`tname`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `class` */

insert  into `class`(`classId`,`cclasname`,`teachername`) values 
(1,'软件01','李老师'),
(2,'艺术01','张老师'),
(3,'体育10','张老师');

/*Table structure for table `course` */

CREATE TABLE `course` (
  `courseId` int(11) NOT NULL,
  `coursename` varchar(20) NOT NULL,
  `coursetype` int(11) default NULL,
  `classname` varchar(20) default NULL,
  PRIMARY KEY  (`courseId`),
  KEY `classname` (`classname`),
  KEY `coursename` (`coursename`),
  CONSTRAINT `course_ibfk_1` FOREIGN KEY (`classname`) REFERENCES `class` (`cclasname`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `course` */

insert  into `course`(`courseId`,`coursename`,`coursetype`,`classname`) values 
(1,'java',1,'软件01'),
(2,'篮球',0,'体育10'),
(3,'舞蹈',0,'艺术01'),
(4,'软件工程',1,'软件01');

/*Table structure for table `student` */

CREATE TABLE `student` (
  `sid` int(10) NOT NULL,
  `sname` varchar(20) NOT NULL,
  `sage` int(5) default NULL,
  `ssex` varchar(5) default NULL,
  `sdept` varchar(10) default NULL,
  `sclas` varchar(20) default NULL,
  `stel` varchar(20) default NULL,
  `saddress` varchar(100) default NULL,
  PRIMARY KEY  (`sid`),
  KEY `student_ibfk_1` (`sclas`),
  CONSTRAINT `student_ibfk_1` FOREIGN KEY (`sclas`) REFERENCES `class` (`cclasname`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `student` */

insert  into `student`(`sid`,`sname`,`sage`,`ssex`,`sdept`,`sclas`,`stel`,`saddress`) values 
(0,'小葱花',19,'女','艺术','艺术01','16535633279','长沙'),
(1,'小北',19,'男','计算机','软件01','16535633258','重庆'),
(2,'景浩',21,'男','艺术','艺术01','16323356328','武汉'),
(3,'小小',21,'女','体育','体育10','16535633258','长沙');

/*Table structure for table `teacher` */

CREATE TABLE `teacher` (
  `tid` int(10) NOT NULL auto_increment,
  `tname` varchar(20) NOT NULL,
  `tsex` varchar(5) default NULL,
  `ttel` varchar(20) default NULL,
  `taddress` varchar(100) default NULL,
  PRIMARY KEY  (`tid`),
  KEY `tname` (`tname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `teacher` */

insert  into `teacher`(`tid`,`tname`,`tsex`,`ttel`,`taddress`) values 
(1,'王老师','男','18336595228','杭州'),
(2,'张老师','女','18336595278','上海'),
(3,'李老师','男','15245214521','厦门');

/*Table structure for table `user` */

CREATE TABLE `user` (
  `id` int(5) NOT NULL auto_increment,
  `username` varchar(10) default NULL,
  `password` varchar(20) default NULL,
  `stuid` int(10) default NULL,
  `teacherid` int(10) default NULL,
  `type` int(5) default NULL,
  PRIMARY KEY  (`id`),
  KEY `stuid` (`stuid`),
  KEY `teacherid` (`teacherid`),
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`stuid`) REFERENCES `student` (`sid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_ibfk_2` FOREIGN KEY (`teacherid`) REFERENCES `teacher` (`tid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `user` */

insert  into `user`(`id`,`username`,`password`,`stuid`,`teacherid`,`type`) values 
(1,'admin','112818',NULL,NULL,1),
(2,'xuesheng','123123',1,NULL,2),
(3,'laoshi','123456',NULL,1,3);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
